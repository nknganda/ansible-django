Django
======

Sets up a django app server with gunicorn and nginx.

Requirements
------------

Tested with Ubuntu 14.04 (64-bit) and Ansible 1.8.

Role Variables
--------------

Variables that can be passed to this role:

    # A list of packages to be installed by apt
    django_system_packages:
      - pip
      - gcc

    # A list of python packages to be installed by pip
    django_python_packages: 
      - virtualenv
      
  
Dependencies
------------

bennojoy.nginx

Example Playbook
-------------------------

    - hosts: servers
      roles:
         - { role: app_server }


License
-------

BSD

Author Information
------------------

sam.kitonyi@gmail.com
